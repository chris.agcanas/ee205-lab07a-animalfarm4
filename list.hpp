///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.hpp
/// @version 1.0
///
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

//using namespace std;

namespace animalfarm{

   class SingleLinkedList{
      protected:
         Node* head = nullptr;
      public:
         const bool empty() const;
         void push_front( Node* newNode );
         Node* pop_front();
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         unsigned int size() const;
   };

}//animalfarm namespace
