///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23 Mar 2021
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdlib>

#include "list.hpp"


namespace animalfarm{

   Node* head = new Node();

   //return true if list is empty.
   const bool SingleLinkedList::empty() const{ 
      return head == NULL;
   }

   //add newNode to front of the list.
   void SingleLinkedList::push_front( Node* newNode ){
      newNode->next = head;
      head = newNode;
   }

   //remove a node from the front of the list.
   Node* SingleLinkedList::pop_front(){
      //if list is empty return nullptr
      if (head == NULL){
         return nullptr;
      }else{
         Node* temp = head;
         head = head -> next;
         return temp;
      }
   }

   Node* SingleLinkedList::get_first() const{
      return head;
   }

   Node* SingleLinkedList::get_next( const Node* currentNode) const{
      return currentNode->next;
   }

   unsigned int SingleLinkedList::size() const{
      unsigned int i;
      Node* index = head;
      for(i = 0; index != NULL; i++){
         index = index -> next;
      }
         return i;
   }

}// animalfarm namespace

