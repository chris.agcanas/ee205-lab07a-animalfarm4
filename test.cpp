//Test file for random generator

#include <iostream>
#include <string>

//header files
#include "animal.hpp"
#include "animalfactory.hpp"
#include "node.hpp"
#include "list.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main(){
   Node node; // Instantiate a node
   SingleLinkedList list; // Instantiate a SingleLinkedList

   cout << "Testing Linked List functions: " << endl;
   cout << "   Before adding animals" << endl;
   cout << "      Is empty: " << boolalpha << list.empty() << endl;
   cout << "      Number of elements: " << list.size() << endl;

   for(auto i = 0; i < 25; i++){
      list.push_front( AnimalFactory::getRandomAnimal());
   }

   cout << endl;
   cout << "   Added animals"<< endl;
   cout << "      Is empty: " << boolalpha << list.empty() << endl;
   cout << "      Number of elements: " << list.size() << endl;
}
