///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file node.cpp
/// @version 1.0
///
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23 Mar 2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "node.hpp"
